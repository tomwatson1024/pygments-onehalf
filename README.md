# pygments-onehalf

[![python: 3.6 | 3.7 | 3.8 | 3.9](https://img.shields.io/badge/python-3.6%20%7C%203.7%20%7C%203.8%20%7C%203.9-blue)](https://www.python.org/)
[![license: MIT](https://img.shields.io/badge/license-MIT-blueviolet.svg)](https://opensource.org/licenses/MIT)
[![PyPI](https://img.shields.io/pypi/v/pygments-onehalf)](https://pypi.org/project/pygments-onehalf/)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/pygments-onehalf)](https://pypistats.org/packages/pygments-onehalf)
[![ci status](https://gitlab.com/tomwatson1024/pygments-onehalf/badges/master/pipeline.svg)](https://gitlab.com/tomwatson1024/pygments-onehalf/commits/master)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)


One Half color scheme for Pygments.

Based on https://github.com/sonph/onehalf, used under the MIT license.

## Installation

### Using Poetry

```
poetry add pygments-onehalf
```

### Using Pip

```
pip install pygments-onehalf
```

## Usage

Provides two pygments styles:
- `onehalf-dark`
- `onehalf-light`
