[tool.poetry]
name = "pygments-onehalf"
version = "1.0.0"
description = "One Half color scheme for Pygments."
authors = ["Tom W <796618-tomwatson1024@users.noreply.gitlab.com>"]
readme = "README.md"
repository = "https://gitlab.com/tomwatson1024/pygments-onehalf"
packages = [{ include = "pygments_onehalf", from = "src" }]

[tool.poetry.dependencies]
python = "^3.6"
Pygments = "^2.10.0"

[tool.poetry.dev-dependencies]
black = "^20.8b1"
flake8 = "^3.9.2"
isort = "^5.8.0"
mypy = "^0.910"
pylint = "^2.9.6"

# Type stubs.
types-setuptools = "^57.0.2"

[build-system]
requires = ["poetry_core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.poetry.plugins."pygments.styles"]
onehalf-dark = "pygments_onehalf:OneHalfDark"
onehalf-light = "pygments_onehalf:OneHalfLight"

[tool.coverage.run]
# Measure branch (rather than line) coverage. See
# https://coverage.readthedocs.io/en/stable/branch.html.
branch = true

source = "pygments_onehalf"

[tool.coverage.report]
show_missing = true

[tool.isort]
profile = "black"

[tool.pylint.messages_control]
disable = [
  # covered by flake8
  "unused-import",
  "unused-variable",

  # covered by mypy
  "import-error",
  "inconsistent-return-statements",
  "no-member",
  "no-name-in-module",

  # covered by pydocstyle
  "missing-class-docstring",
  "missing-function-docstring",
  "missing-module-docstring",

  # too coarse to be useful
  "design",
  "similarities",

  # don't enforce a naming convention
  "invalid-name",

  # produces false positives e.g. when implementing abstract methods
  "no-self-use",

  # it's sometimes clearer to use `else` even after a return
  "no-else-return",
]

[tool.pylint.reports]
output-format = "colorized"
score = "n"

[tool.mypy]
files = ["src"]

# Require all definitions to be typed. The `Any` type may be used to explicitly
# opt-out of type checking.
check_untyped_defs = true
disallow_untyped_defs = true

# Require type parameters of generic types to be explicitly specified. The
# `Any` type may be used where a more specific type is not appropriate.
disallow_any_generics = true

# Disable implicit re-exporting of names imported by a module. Names may be
# explicitly exported using `__all__`.
implicit_reexport = false

# Error codes can be used to write more specific `type: ignore` comments.
show_error_codes = true

# These dependencies don't provide type hints; mypy complains unless we
# explicitly ignore them.
[[tool.mypy.overrides]]
module = "pygments.*"
ignore_missing_imports = true
