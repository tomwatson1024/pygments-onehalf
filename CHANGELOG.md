# Changelog

This file documents all notable changes to the project.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
Commit messages and changelog entries should follow the [Conventional Commits](https://www.conventionalcommits.org) specification.

## [Unreleased]

## [1.0.0] - 2022-10-18

### Added
- feat: Stabilize as version 1.0.0.

## [0.1.2] - 2022-10-18

### Fixed
- fix: Fix exception when running in an environment without setuptools.

## [0.1.1] - 2021-08-31

### Fixed
- fix: Relax minimum Python version constraint from `3.6.2` to `3.6`.

## [0.1.0]

### Added

- Initial version

[Unreleased]: https://gitlab.com/tomwatson1024/pygments-onehalf/compare/1.0.0...master
[1.0.0]: https://gitlab.com/tomwatson1024/pygments-onehalf/compare/0.1.2...1.0.0
[0.1.2]: https://gitlab.com/tomwatson1024/pygments-onehalf/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/tomwatson1024/pygments-onehalf/compare/0.1.1..0.1.0
[0.1.0]: https://gitlab.com/tomwatson1024/pygments-onehalf/-/tags/0.1.0
