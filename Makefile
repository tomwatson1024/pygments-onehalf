SHELL = /bin/bash -o pipefail
ALL_FILES = src

.PHONY: all
all: style lint build


.PHONY: lint
lint: mypy pylint

.PHONY: mypy
mypy:
# Editors use mypy's incremental mode to provide error squiggles, reading config
# from setup.cfg. Certain options are not suitable for use in incremental mode,
# or produce output not suitable for error squiggles.
# Enable those options here instead of in setup.cfg.
	poetry run mypy \
	  --pretty \
	  --show-error-context \
	  --warn-redundant-casts \
	  --warn-unused-ignores \
	  --warn-unused-configs

.PHONY: pylint
pylint:
	poetry run pylint $(ALL_FILES)


.PHONY: style
style: flake8 isort_check black_check

.PHONY: autofmt
autofmt: isort black

.PHONY: flake8
flake8:
	poetry run flake8 $(ALL_FILES)

BLACK_COMMAND := poetry run black $(ALL_FILES)

.PHONY: black_check
black_check:
	$(BLACK_COMMAND) --check

.PHONY: black
black:
	$(BLACK_COMMAND)

ISORT_COMMAND := poetry run isort $(ALL_FILES)

.PHONY: isort_check
isort_check:
	$(ISORT_COMMAND) --check-only

.PHONY: isort
isort:
	$(ISORT_COMMAND)

.PHONY: build
build:
	poetry build
